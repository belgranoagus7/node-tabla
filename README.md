# Notas:
Este es mi primer programa en node

```
Options:
      --help     Show help                                      [boolean]
      --version  Show version number                            [boolean]
  -b, --base     Es la base de la tabla de multiplicar          [number] [required]
  -l, --listar   Muestra la tabla por consola                   [boolean] [default: false]
  -m, --maximum  Máximo de números que quieres multiplicar      [number] [default: 10]  
```