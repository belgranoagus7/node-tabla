const fs = require('fs');
const colors = require('colors');

const crearArchivo = async (base = 5, listar = false, max=10) => {

    try {


        let salida = '';
        let consola = '';

        for (let i = 1; i <= max; i++) {
            salida += (`${base} x ${i} = ${base * i}\n`);
            consola += (`${base} x ${i} = ${colors.bold.blue(base * i)}\n`);
        }

        if (listar) {

            console.log(colors.green('======================='));
            console.log(colors.white.underline('TABLA DEL:'), colors.brightYellow(base));
            console.log(colors.green('======================='));
            console.log(consola);
        }


        fs.writeFileSync(`./salida/tabla-${base}.txt`, salida);

        return `tabla-${base}.txt`;

    } catch (err) {
        throw err;
    }


}

module.exports = {
    crearArchivo,
}